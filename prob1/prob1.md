### [Main Page](../README.md)
### [โจทย์ปัญหา:](prob1TH.md)
### Problem statement: 

Given a point charge of $`Q`$ Coulomb at point $`(x_o, y_o, z_o)`$ and a conducting plane on $`xy`$ plane. Find surface charge density on the conducting plane at the origin $`(0,0,0)`$.
### [วิธีทำ TH](prob1solutionTH.md)
### [Solutions EN](prob1solutionEN.md)
<img src="asset/prob1.png" width="50%" height="50%">
